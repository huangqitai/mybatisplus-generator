package com.hqt.mybatisplusgenerator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;


public class MyBatisPlusGenerator {
    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");
        String src = projectPath + "/src/main/java";
        String mapper = projectPath + "/src/main/resources/mapper/";
        DataSourceConfig.Builder dataSourceBuilder = new DataSourceConfig.Builder("jdbc:postgresql://10.20.40.53:5432/nmjf445303",
                "postgres", "postgres");
        dataSourceBuilder.schema("zwhj");
        FastAutoGenerator fastAutoGenerator = FastAutoGenerator.create(dataSourceBuilder);
        fastAutoGenerator.globalConfig(builder -> {
                    builder.disableOpenDir().author("黄启太") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(src); // 指定输出目录
                });
        fastAutoGenerator.packageConfig(builder -> {
                    builder.parent("com.hqt.mybatisplusgenerator") // 设置父包名
                            .moduleName("muser") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, mapper)); // 设置mapperXml生成路径
                });
        fastAutoGenerator.strategyConfig(builder -> {
                    builder.addInclude("up_data_reconciliation_v4") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_","m_"); // 设置过滤表前缀
                });
        fastAutoGenerator.templateEngine(new FreemarkerTemplateEngine()); // 使用Freemarker引擎模板，默认的是Velocity引擎模板
        fastAutoGenerator.execute();
    }

}
