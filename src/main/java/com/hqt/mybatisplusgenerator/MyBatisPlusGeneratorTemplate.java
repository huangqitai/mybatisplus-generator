package com.hqt.mybatisplusgenerator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class MyBatisPlusGeneratorTemplate {
    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");
        String src = projectPath + "/src/main/java";
        String mapper = projectPath + "/src/main/resources/mapper/";
        //数据源配置
        DataSourceConfig.Builder dataSourceBuilder = new DataSourceConfig.Builder("jdbc:postgresql://10.20.40.53:5432/nmjf445303",
                "postgres", "postgres");
        dataSourceBuilder.schema("zwhj");
        FastAutoGenerator fastAutoGenerator = FastAutoGenerator.create(dataSourceBuilder);
        //全局配置
        /**
         * disableOpenDir	禁止打开输出目录	默认值:true
         * outputDir(String)	指定输出目录	/opt/baomidou/ 默认值: windows:D:// linux or mac : /tmp
         * author(String)	作者名	baomidou 默认值:作者
         * enableKotlin	开启 kotlin 模式	默认值:false
         * enableSwagger	开启 swagger 模式	默认值:false
         * dateType(DateType)	时间策略	DateType.ONLY_DATE 默认值: DateType.TIME_PACK
         * commentDate(String)	注释日期	默认值: yyyy-MM-dd
         */
        fastAutoGenerator.globalConfig(builder -> {
                    builder
                            .disableOpenDir()
                            .fileOverride()
                            .outputDir(src)
                            .author("黄启太")
                            .enableSwagger()
                            .dateType(DateType.TIME_PACK)
                            .commentDate("yyyy-MM-dd");
                });
        //包配置
        /**
         * parent(String)	父包名	默认值:com.baomidou
         * moduleName(String)	父包模块名	默认值:无
         * entity(String)	Entity 包名	默认值:entity
         * service(String)	Service 包名	默认值:service
         * serviceImpl(String)	Service Impl 包名	默认值:service.impl
         * mapper(String)	Mapper 包名	默认值:mapper
         * xml(String)	Mapper XML 包名	默认值:mapper.xml
         * controller(String)	Controller 包名	默认值:controller
         * other(String)	自定义文件包名	输出自定义文件时所用到的包名
         * pathInfo(Map<OutputFile, String>)	路径配置信息	Collections.singletonMap(OutputFile.mapperXml, "D://")
         */
        fastAutoGenerator.packageConfig(builder -> {
                    builder.parent("com.hqt.mybatisplusgenerator") // 设置父包名
                            .moduleName("muser") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, mapper)); // 设置mapperXml生成路径
                });
        //策略配置
        /**
         * enableCapitalMode	开启大写命名	默认值:false
         * enableSkipView	开启跳过视图	默认值:false
         * disableSqlFilter	禁用 sql 过滤	默认值:true，语法不能支持使用 sql 过滤表的话，可以考虑关闭此开关
         * enableSchema	启用 schema	默认值:false，多 schema 场景的时候打开
         * likeTable(LikeTable)	模糊表匹配(sql 过滤)	likeTable 与 notLikeTable 只能配置一项
         * notLikeTable(LikeTable)	模糊表排除(sql 过滤)	likeTable 与 notLikeTable 只能配置一项
         * addInclude(String...)	增加表匹配(内存过滤)	include 与 exclude 只能配置一项
         * addExclude(String...)	增加表排除匹配(内存过滤)	include 与 exclude 只能配置一项
         * addTablePrefix(String...)	增加过滤表前缀
         * addTableSuffix(String...)	增加过滤表后缀
         * addFieldPrefix(String...)	增加过滤字段前缀
         * addFieldSuffix(String...)	增加过滤字段后缀
         * entityBuilder	实体策略配置
         * controllerBuilder	controller 策略配置
         * mapperBuilder	mapper 策略配置
         * serviceBuilder	service 策略配置
         */
        /**
         * Entity 策略配置
         * 方法	说明	示例
         * nameConvert(INameConvert)	名称转换实现
         * superClass(Class<?>)	设置父类	BaseEntity.class
         * superClass(String)	设置父类	com.baomidou.global.BaseEntity
         * disableSerialVersionUID	禁用生成 serialVersionUID	默认值:true
         * enableFileOverride	覆盖已生成文件	默认值:false
         * enableColumnConstant	开启生成字段常量	默认值:false
         * enableChainModel	开启链式模型	默认值:false
         * enableLombok	开启 lombok 模型	默认值:false
         * enableRemoveIsPrefix	开启 Boolean 类型字段移除 is 前缀	默认值:false
         * enableTableFieldAnnotation	开启生成实体时生成字段注解	默认值:false
         * enableActiveRecord	开启 ActiveRecord 模型	默认值:false
         * versionColumnName(String)	乐观锁字段名(数据库)
         * versionPropertyName(String)	乐观锁属性名(实体)
         * logicDeleteColumnName(String)	逻辑删除字段名(数据库)
         * logicDeletePropertyName(String)	逻辑删除属性名(实体)
         * naming	数据库表映射到实体的命名策略	默认下划线转驼峰命名:NamingStrategy.underline_to_camel
         * columnNaming	数据库表字段映射到实体的命名策略	默认为 null，未指定按照 naming 执行
         * addSuperEntityColumns(String...)	添加父类公共字段
         * addIgnoreColumns(String...)	添加忽略字段
         * addTableFills(IFill...)	添加表字段填充
         * addTableFills(List<IFill>)	添加表字段填充
         * idType(IdType)	全局主键类型
         * convertFileName(ConverterFileName)	转换文件名称
         * formatFileName(String)	格式化文件名称
         */
        fastAutoGenerator.strategyConfig(builder -> {
                    builder
                            .addInclude("up_data_reconciliation_v4") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_","m_") // 设置过滤表前缀
                            //设定实体配置
                            .entityBuilder()
                            .enableLombok();
                });

        fastAutoGenerator.templateConfig(builder -> {
            builder.mapperXml("/template/mapper.xml");
        });
        fastAutoGenerator.templateEngine(new VelocityTemplateEngine()); // 使用Freemarker引擎模板，默认的是Velocity引擎模板
        fastAutoGenerator.execute();
    }

}
